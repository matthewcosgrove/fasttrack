package taskboot

import (
	"fmt"
	"os"
	"path"
	"strings"

	"github.com/kelseyhightower/envconfig"
	"github.com/spf13/afero"
)

var AppFs = afero.NewOsFs()

type MandatoryEnvVars struct {
	TaskCurrentNamespace string `required:"true" split_words:"true"`
	RepoRootDir          string `required:"true" split_words:"true"`
}

const envVarPrefix = "FASTTRACK"
const envVarKeyTaskCurrent = "FASTTRACK_TASK_CURRENT_NAMESPACE" // Duplicated for other checks, so sue me
const envVarKeyRepoRootDir = "FASTTRACK_REPO_ROOT_DIR"          // Duplicated for other checks, so sue me

const CITasksPathStem = "ci/tasks/"

func CreateTaskFolderIfNotExists() error {

	// 1) All Env vars are checked first via https://github.com/kelseyhightower/envconfig
	var envVars MandatoryEnvVars
	err := envconfig.Process(envVarPrefix, &envVars)
	if err != nil {
		return &MandatoryEnvVarsError{
			Err: fmt.Errorf("Error parsing env vars: %v\n", err),
		}
	}

	repoRootDir := envVars.RepoRootDir
	taskCurrent := envVars.TaskCurrentNamespace
	// 2) Then we check to see if they are empty ourselves
	if taskCurrent == "" {
		return fmt.Errorf("Mandatory env var %s is empty\n", envVarKeyTaskCurrent)
	}
	if repoRootDir == "" {
		return fmt.Errorf("Mandatory env var %s is empty\n", envVarKeyRepoRootDir)
	}

	// 3) Then we check to see they are valid
	if !strings.Contains(taskCurrent, "/") {
		return fmt.Errorf("Mandatory env var %s should be of the format \"job-name/task-name\"\n", envVarKeyTaskCurrent)
	}

	filepath := path.Join(repoRootDir, CITasksPathStem)
	fmt.Printf("Ensuring path stem exists: %s\n", filepath)

	err = AppFs.MkdirAll(filepath, os.ModePerm)
	if err != nil {
		return &MakingDirectoryError{
			Err: fmt.Errorf("Error making dir %s: %v\n", filepath, err),
		}
	}

	taskCurrentFullPath := path.Join(filepath, taskCurrent)
	fmt.Printf("Checking if %s already exists\n", taskCurrentFullPath)
	exists, err := afero.DirExists(AppFs, taskCurrentFullPath)
	if err != nil {
		return &CheckingDirectoryError{
			Err: fmt.Errorf("Error checking dir %s: %v\n", taskCurrentFullPath, err),
		}
	}
	if exists {
		return fmt.Errorf("Bailing out! Directory already exists %s\n", taskCurrentFullPath)
	}
	fmt.Printf("%s does not already exist, continuing..\n", taskCurrentFullPath)
	err = AppFs.MkdirAll(taskCurrentFullPath, os.ModePerm)
	if err != nil {
		return &MakingDirectoryError{
			Err: fmt.Errorf("Error making dir %s: %v\n", taskCurrentFullPath, err),
		}
	}
	fmt.Printf("Created directories: %s\n", taskCurrentFullPath)
	return nil
}

type MandatoryEnvVarsError struct {
	Err error
}

func (e *MandatoryEnvVarsError) Error() string { return e.Err.Error() }

type CheckingDirectoryError struct {
	Err error
}

func (e *CheckingDirectoryError) Error() string { return e.Err.Error() }

type MakingDirectoryError struct {
	Err error
}

func (e *MakingDirectoryError) Error() string { return e.Err.Error() }
