package taskboot_test

import (
	"fmt"
	"os"
	"path"
	"path/filepath"
	"reflect"
	"runtime"
	"strings"
	"testing"

	"github.com/spf13/afero"
	. "gitlab.com/matthewcosgrove/fasttrack/taskboot"
) // fully import boot package to allow override of var AppFs = afero.NewMemMapFs()

const envVarKeyTaskCurrent = "FASTTRACK_TASK_CURRENT_NAMESPACE"
const envVarKeyRepoRootDir = "FASTTRACK_REPO_ROOT_DIR"

const repoRootDir = "/some/path/for/this/test"

// set up for each test with https://golang.org/pkg/testing/#hdr-Main
func TestMain(m *testing.M) {
	AppFs = afero.NewMemMapFs()
	os.Unsetenv(envVarKeyTaskCurrent)
	os.Unsetenv(envVarKeyRepoRootDir)
	os.Exit(m.Run())
}

// IMPORTANT: Order matters and influences how we set up for tests
// 1) All Env vars are checked first via https://github.com/kelseyhightower/envconfig
// 2) Then we check to see if they are empty ourselves
// 3) Then we check to see they are valid

// Potential gotchyas..
// https://github.com/spf13/afero/issues/149#issuecomment-514085465
// => Mkdir() works in memory but fails on real os
// also see..
// https://github.com/spf13/afero/issues/209

func TestErrorsIfTaskCurrentEnvVarNotSet(t *testing.T) {
	err := CreateTaskFolderIfNotExists()
	assertErrorReturned(t, err)
	want := "required key " + envVarKeyTaskCurrent + " missing value"
	got := err.Error()
	assertContains(t, got, want)
	t.Logf("[SUCCESS] Expected error returned with message: %s\n", err)
}
func TestErrorsIfRepoRootDirEnvVarNotSet(t *testing.T) {
	os.Setenv(envVarKeyTaskCurrent, "valid-job/task-combo")
	err := CreateTaskFolderIfNotExists()
	assertErrorReturned(t, err)
	want := "required key " + envVarKeyRepoRootDir + " missing value"
	got := err.Error()
	assertContains(t, got, want)
	t.Logf("[SUCCESS] Expected error returned with message: %s\n", err)
}
func TestErrorsIfTaskCurrentEnvVarSetButEmpty(t *testing.T) {
	os.Setenv(envVarKeyRepoRootDir, repoRootDir)
	os.Setenv(envVarKeyTaskCurrent, "")
	err := CreateTaskFolderIfNotExists()
	assertErrorReturned(t, err)
	want := "Mandatory env var " + envVarKeyTaskCurrent + " is empty"
	got := err.Error()
	assertContains(t, got, want)
	t.Logf("[SUCCESS] Expected error returned with message: %s\n", err)
}
func TestErrorsIfRepoRootDirEnvVarSetButEmpty(t *testing.T) {
	os.Setenv(envVarKeyRepoRootDir, "")
	os.Setenv(envVarKeyTaskCurrent, "valid-job/task-combo")
	err := CreateTaskFolderIfNotExists()
	assertErrorReturned(t, err)
	want := "Mandatory env var " + envVarKeyRepoRootDir + " is empty"
	got := err.Error()
	assertContains(t, got, want)
	t.Logf("[SUCCESS] Expected error returned with message: %s\n", err)
}
func TestErrorsIfTaskCurrentEnvVarNotValid(t *testing.T) {
	os.Setenv(envVarKeyRepoRootDir, repoRootDir)
	os.Setenv(envVarKeyTaskCurrent, "missing_forward_slash_in_env_var")
	err := CreateTaskFolderIfNotExists()
	assertErrorReturned(t, err)
	want := "Mandatory env var " + envVarKeyTaskCurrent + " should be of the format \"job-name/task-name\""
	got := err.Error()
	assertContains(t, got, want)
	t.Logf("[SUCCESS] Expected error returned with message: %s\n", err)
}
func TestErrorsIfTaskCurrentFolderAlreadyExists(t *testing.T) {
	newTaskDirs := "existing-job-name/existing-task-name"
	t.Logf("Attempting to create directories for: %s\n", newTaskDirs)
	fullPathOfDirectoriesToCreateBeforeTest := path.Join(repoRootDir, CITasksPathStem, newTaskDirs)
	AppFs.MkdirAll(fullPathOfDirectoriesToCreateBeforeTest, 0777)
	t.Logf("Created directories for test: %s\n", fullPathOfDirectoriesToCreateBeforeTest)
	os.Setenv(envVarKeyRepoRootDir, repoRootDir)
	os.Setenv(envVarKeyTaskCurrent, newTaskDirs)
	err := CreateTaskFolderIfNotExists()
	assertErrorReturned(t, err)
	// assertErrorReturnedIsExpectedTypeForMakingDirectoryError(t, err)
	want := "Bailing out! Directory already exists " + fullPathOfDirectoriesToCreateBeforeTest
	got := err.Error()
	assertContains(t, got, want)
	t.Logf("[SUCCESS] Expected error returned with message: %s\n", err)
}
func TestNoErrorIfJobNamePartOfTaskCurrentFolderAlreadyExists(t *testing.T) {
	jobDirOnly := "existing-job-name"
	t.Logf("Attempting to create directories for: %s\n", jobDirOnly)
	fullPathOfDirectoriesToCreateBeforeTest := path.Join(repoRootDir, CITasksPathStem, jobDirOnly)
	AppFs.Mkdir(fullPathOfDirectoriesToCreateBeforeTest, 0777)
	t.Logf("Created directories for test: %s\n", fullPathOfDirectoriesToCreateBeforeTest)
	os.Setenv(envVarKeyTaskCurrent, jobDirOnly+"/new-task-name")
	err := CreateTaskFolderIfNotExists()
	ok(t, err)
	assertErrorReturnedIsNotUnexpected(t, err)
}
func TestTaskCurrentFoldersGetsCreated(t *testing.T) {
	newTaskDirs := "new-job-name/new-task-name"
	os.Setenv(envVarKeyRepoRootDir, repoRootDir)
	os.Setenv(envVarKeyTaskCurrent, newTaskDirs)
	err := CreateTaskFolderIfNotExists()
	ok(t, err)
	assertErrorReturnedIsNotUnexpected(t, err)
	fullPathOfNewDirs := path.Join(repoRootDir, CITasksPathStem, newTaskDirs)
	fi, err := AppFs.Stat(fullPathOfNewDirs)
	if err != nil {
		t.Errorf("Could not stat %s, got error: %v\n", fullPathOfNewDirs, err)
	} else {
		t.Logf("[SUCCESS] Directory %v created succssfully and isDir=%v\n", fi.Name(), fi.IsDir())
	}
}

func assertContains(tb testing.TB, got string, want string) {
	if !strings.Contains(got, want) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\tgot: %#v\n\n\twant: %#v\033[39m\n\n", filepath.Base(file), line, got, want)
		tb.FailNow()
	}
}

func assertErrorReturnedIsExpectedTypeForMakingDirectoryError(tb testing.TB, err error) {
	if err != nil {
		errMaking, ok := err.(*MakingDirectoryError)
		if !ok {
			_, file, line, _ := runtime.Caller(1)
			fmt.Printf("\033[31m%s:%d: unexpected error: %s\033[39m\n\n", filepath.Base(file), line, errMaking.Error())
			tb.FailNow()
		}
	}
}

func assertErrorReturnedIsNotUnexpected(tb testing.TB, err error) {
	if err != nil {
		errChecking, ok := err.(*CheckingDirectoryError)
		if ok {
			_, file, line, _ := runtime.Caller(1)
			fmt.Printf("\033[31m%s:%d: unexpected error: %s\033[39m\n\n", filepath.Base(file), line, errChecking.Error())
			tb.FailNow()
		}
		errMaking, ok := err.(*MakingDirectoryError)
		if ok {
			_, file, line, _ := runtime.Caller(1)
			fmt.Printf("\033[31m%s:%d: unexpected error: %s\033[39m\n\n", filepath.Base(file), line, errMaking.Error())
			tb.FailNow()
		}
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d: unexpected error: %s\033[39m\n\n", filepath.Base(file), line, err.Error())
		tb.FailNow()
	}
}

// helper funcs courtesy of https://github.com/benbjohnson/testing

// assertErrorReturned fails the test if an err is nil.
func assertErrorReturned(tb testing.TB, err error) {
	if err == nil {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d: Expected error but got nil\033[39m\n\n", filepath.Base(file), line)
		tb.FailNow()
	}
}

// assert fails the test if the condition is false.
func assert(tb testing.TB, condition bool, msg string, v ...interface{}) {
	if !condition {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d: "+msg+"\033[39m\n\n", append([]interface{}{filepath.Base(file), line}, v...)...)
		tb.FailNow()
	}
}

// ok fails the test if an err is not nil.
func ok(tb testing.TB, err error) {
	if err != nil {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d: unexpected error: %s\033[39m\n\n", filepath.Base(file), line, err.Error())
		tb.FailNow()
	}
}

// equals fails the test if exp is not equal to act.
func equals(tb testing.TB, exp, act interface{}) {
	if !reflect.DeepEqual(exp, act) {
		_, file, line, _ := runtime.Caller(1)
		fmt.Printf("\033[31m%s:%d:\n\n\texp: %#v\n\n\tgot: %#v\033[39m\n\n", filepath.Base(file), line, exp, act)
		tb.FailNow()
	}
}
