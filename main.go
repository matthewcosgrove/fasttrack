package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/matthewcosgrove/fasttrack/taskboot"
)

func main() {
	taskPtr := flag.String("t", "", "Task namespace (which fasttrack defines as the combination of job name and task name) to parse which must be in the form \"job-name/task-name\" where common tasks should use \"common/task-name\" instead. (Required)")
	flag.Parse()
	if *taskPtr == "" {
		flag.PrintDefaults()
		os.Exit(1)
	}
	os.Setenv("FASTTRACK_TASK_CURRENT_NAMESPACE", *taskPtr)
	err := taskboot.CreateTaskFolderIfNotExists()
	if err != nil {
		fmt.Printf("Doh! %v\n", err)
	}
}
