module gitlab.com/matthewcosgrove/fasttrack

go 1.14

require (
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/pkg/errors v0.9.1
	github.com/spf13/afero v1.2.2
)
