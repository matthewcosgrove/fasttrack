# Fast Track

Convention over configuration approach to managing Concourse tasks for streamlining the development workflow with the main objective of ensuring that testing your tasks before integrating them into a pipeline is a breeze.

<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS4r3RcFYlFbmgpqAZYtyL9ZJ9rTrOA3aGlOUuTeilKhEbbVALc&usqp=CAU" alt="Biggles" width="200"/>

*"VIPs use Fast Track because TDD in IaC becomes as easy as ABC!"* - Biggles
